var React = require('react');
var DropDown = require('./dropdown');


var options = {
  title: "Choose a dessert",
  items: [
    'apple',
    'banana',
    'cream',
    ]
}

var element = React.createElement(DropDown,options);
React.render(element, document.querySelector('.target'));
